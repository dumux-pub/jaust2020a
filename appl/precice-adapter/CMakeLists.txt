add_subdirectory(include)
add_subdirectory(src)
add_subdirectory(test)

install(FILES
        README.md
        README.md~
        change-git-remote.sh
        DESTINATION ${CMAKE_INSTALL_INCLUDEDIR}/appl/precice-adapter)
