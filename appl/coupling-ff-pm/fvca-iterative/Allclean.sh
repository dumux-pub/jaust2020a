#! /usr/bin/env bash

rm -rf "precice-run/"
rm -f "${ff_solver}.log" "${pm_solver}.log"
rm -f precice-*.log
rm -f precice-*.json
rm -f *.vtu *.pvd *.log