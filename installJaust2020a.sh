#!/bin/bash

################################################################################
# This script installs the module "Jaust2020a" together with all dependencies.

# defines a function to exit with error message
exitWith ()
{
    echo "\n$1"
    exit 1
}

# Everything will be installed inside the folder from which the script is executed.

# Jaust2020a
# master # 702f61fc1793de3e76834a9893a82c383dacd6df # 2021-03-09 17:23:22 +0100 # Alexander Jaust
if ! git clone https://git.iws.uni-stuttgart.de/dumux-pub/Jaust2020a.git; then exitWith "-- Error: failed to clone Jaust2020a."; fi
if ! cd Jaust2020a; then exitWith "-- Error: could not enter folder Jaust2020a."; fi
if ! git checkout master; then exitWith "-- Error: failed to check out branch master in module Jaust2020a."; fi
if ! git reset --hard 702f61fc1793de3e76834a9893a82c383dacd6df; then exitWith "-- Error: failed to check out commit 702f61fc1793de3e76834a9893a82c383dacd6df in module Jaust2020a."; fi
echo "-- Successfully set up the module Jaust2020a\n"
cd ..

# dumux
# feature/ff-stokes-reverse-coupling # 08cdea1105418767de42e1180df3bb8f47edd45e # 2021-01-28 15:50:21 +0100 # Alexander Jaust
if ! git clone https://git.iws.uni-stuttgart.de/dumux-repositories/dumux.git; then exitWith "-- Error: failed to clone dumux."; fi
if ! cd dumux; then exitWith "-- Error: could not enter folder dumux."; fi
if ! git checkout feature/ff-stokes-reverse-coupling; then exitWith "-- Error: failed to check out branch feature/ff-stokes-reverse-coupling in module dumux."; fi
if ! git reset --hard 08cdea1105418767de42e1180df3bb8f47edd45e; then exitWith "-- Error: failed to check out commit 08cdea1105418767de42e1180df3bb8f47edd45e in module dumux."; fi
echo "-- Successfully set up the module dumux\n"
cd ..

# dune-geometry
# releases/2.7 # 2a66558feebde80a5454845495f21a9643b3c76b # 2020-08-11 11:45:36 +0000 # René Heß
if ! git clone https://gitlab.dune-project.org/core/dune-geometry.git; then exitWith "-- Error: failed to clone dune-geometry."; fi
if ! cd dune-geometry; then exitWith "-- Error: could not enter folder dune-geometry."; fi
if ! git checkout releases/2.7; then exitWith "-- Error: failed to check out branch releases/2.7 in module dune-geometry."; fi
if ! git reset --hard 2a66558feebde80a5454845495f21a9643b3c76b; then exitWith "-- Error: failed to check out commit 2a66558feebde80a5454845495f21a9643b3c76b in module dune-geometry."; fi
echo "-- Successfully set up the module dune-geometry\n"
cd ..

# dune-localfunctions
# releases/2.7 # f93600dfd94fa0124ead0d56101f0482ed049e15 # 2020-08-31 08:48:42 +0000 # Oliver Sander
if ! git clone https://gitlab.dune-project.org/core/dune-localfunctions.git; then exitWith "-- Error: failed to clone dune-localfunctions."; fi
if ! cd dune-localfunctions; then exitWith "-- Error: could not enter folder dune-localfunctions."; fi
if ! git checkout releases/2.7; then exitWith "-- Error: failed to check out branch releases/2.7 in module dune-localfunctions."; fi
if ! git reset --hard f93600dfd94fa0124ead0d56101f0482ed049e15; then exitWith "-- Error: failed to check out commit f93600dfd94fa0124ead0d56101f0482ed049e15 in module dune-localfunctions."; fi
echo "-- Successfully set up the module dune-localfunctions\n"
cd ..

# dune-common
# releases/2.7 # aa689abba532f40db8f5663fa379ea77211c1953 # 2020-11-10 13:36:21 +0000 # Christian Engwer
if ! git clone https://gitlab.dune-project.org/core/dune-common.git; then exitWith "-- Error: failed to clone dune-common."; fi
if ! cd dune-common; then exitWith "-- Error: could not enter folder dune-common."; fi
if ! git checkout releases/2.7; then exitWith "-- Error: failed to check out branch releases/2.7 in module dune-common."; fi
if ! git reset --hard aa689abba532f40db8f5663fa379ea77211c1953; then exitWith "-- Error: failed to check out commit aa689abba532f40db8f5663fa379ea77211c1953 in module dune-common."; fi
echo "-- Successfully set up the module dune-common\n"
cd ..

# dune-istl
# releases/2.7 # 375e9b4b0d14961d496588b98468083c82d50fe8 # 2020-08-11 10:57:38 +0000 # René Heß
if ! git clone https://gitlab.dune-project.org/core/dune-istl.git; then exitWith "-- Error: failed to clone dune-istl."; fi
if ! cd dune-istl; then exitWith "-- Error: could not enter folder dune-istl."; fi
if ! git checkout releases/2.7; then exitWith "-- Error: failed to check out branch releases/2.7 in module dune-istl."; fi
if ! git reset --hard 375e9b4b0d14961d496588b98468083c82d50fe8; then exitWith "-- Error: failed to check out commit 375e9b4b0d14961d496588b98468083c82d50fe8 in module dune-istl."; fi
echo "-- Successfully set up the module dune-istl\n"
cd ..

# dune-grid
# releases/2.7 # 369cac45cf0fb27eb9181018509d74c38f07fdf6 # 2020-10-21 05:15:43 +0000 # Christoph Grüninger
if ! git clone https://gitlab.dune-project.org/core/dune-grid.git; then exitWith "-- Error: failed to clone dune-grid."; fi
if ! cd dune-grid; then exitWith "-- Error: could not enter folder dune-grid."; fi
if ! git checkout releases/2.7; then exitWith "-- Error: failed to check out branch releases/2.7 in module dune-grid."; fi
if ! git reset --hard 369cac45cf0fb27eb9181018509d74c38f07fdf6; then exitWith "-- Error: failed to check out commit 369cac45cf0fb27eb9181018509d74c38f07fdf6 in module dune-grid."; fi
echo "-- Successfully set up the module dune-grid\n"
cd ..

echo "-- All modules haven been cloned successfully. Configuring project..."
if ! ./dune-common/bin/dunecontrol --opts=Jaust2020a/test/cmake-test.opts all; then exitWith "--Error: could not configure project"; fi

echo "-- Configuring successful. Compiling applications..."
if ! cd Jaust2020a/build-cmake; then exitWith "--Error: could not enter build directory at Jaust2020a/build-cmake"; fi
if ! make build_tests; then exitWith "--Error: applications could not be compiled. Please try to compile them manually."; fi
