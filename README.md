# Summary

This is the DuMuX module containing the code for producing the results of:

* Jaust A., Weishaupt K., Mehl M., Flemisch B. (2020) *Partitioned Coupling Schemes for Free-Flow and Porous-Media Applications with Sharp Interfaces*. In: Klöfkorn R., Keilegavlen E., Radu F., Fuhrmann J. (eds) Finite Volumes for Complex Applications IX - Methods, Theoretical Aspects, Examples. FVCA 2020. Springer Proceedings in Mathematics & Statistics, vol 323. Springer, Cham. [https://doi.org/10.1007/978-3-030-43651-3_57](https://doi.org/10.1007/978-3-030-43651-3_57)

# Installation


__Dependencies__
This module [requires](https://dune-project.org/doc/installation/)
* a standard c++17 compliant compiler (e.g. gcc or clang)
* CMake
* pkg-config
* [preCICE](https://www.precice.org/) 1.6.0 or 1.6.1
    * Only mandatory dependencies of preCICE, e.g. Boost >= 1.65.1, are needed.
    * Using 1.6.X is mandatory to reproduce the results of the correspnding publication. Older versions of preCICE may work, but newer releases, i.e. preCICE 2.X.X, will not work as the preCICE API changed.


The easiest way to install `DuMuX` and necessary DUNE modules is to use the script `installJaust2020a.sh` provided in this repository.
Using `wget`, you can simply install all dependent modules by typing:

```sh
wget https://git.iws.uni-stuttgart.de/dumux-pub/Jaust2020a.git/installJaust2020a.sh
chmod u+x installJaust2020a.sh
./installJaust2020a.sh
```

preCICE needs to be installed seperately from source or via one of the provided binary packages.

## Run simulations

On can run simulations using the monolithic coupling or the partitioned/iterative coupling.
### Monolithic coupling simulations

The executables are built in `jaust2020a/build-cmake/appl/coupling-ff-pm/fvca-monolithic/`. The simulation can be run as

```bash
cd jaust2020a/build-cmake/appl/coupling-ff-pm/fvca-monolithic/
make fvca-monolithic
./fvca-monolithic params.input
```

### Partitioned/Iterative coupling simulations

The executables are built in `jaust2020a/build-cmake/appl/coupling-ff-pm/fvca-iterative/`

```bash
cd jaust2020a/build-cmake/appl/coupling-ff-pm/fvca-iterative/
make fvca-iterative-ff fvca-iterative-pm
```
To start both solvers conveniently we provide a helper script `Allrun.sh` that takes a preCICE configuration file as argument. You can start the *parallel-implicit* coupling with

```
./Allrun.sh  precice-config-parallel-implicit.xml
```
See also the other XML-files in this directory for further preCICE configurations. The script `Allclean.sh` will remove all simulation results.

### Testing

For the monolithic and the iterative/partitioned coupling we provide tests that you can invoke with `ctest` in the respective directory (after the executables have been built). You find the reference results in `jaust2020a/test/coupling-ff-pm/reference-solutions/`. The monolithic results in the subdirectory `monolithic` refer to the solutions as presented in the paper while `monolithic-new` are referring to the results obtained from the more recent version of DuMuX this repository is based on.
### Visualization

You may use [ParaView](https://www.paraview.org/) to visualize the results by opening the two `.pvd` files. The `.log` files contain additional data such as the needed number of coupling iterations and the coupling residuals.
