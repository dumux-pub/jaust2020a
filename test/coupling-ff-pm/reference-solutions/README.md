# Short overview over reference solutions

The reference solutions are based on the data used for publication at FVCA IX

## Problem setup

- 40x40 mesh elements
- Stokes flow
- Pressure difference 1e-9
- Permeability 1e-6
- Porosity 0.4
- AlphaBeaversJoseph 1.0
## Iterative coupling

- Coupling convergence limit: 1e-8
